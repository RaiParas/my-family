import 'package:flutter/material.dart';
import 'package:my_family/Screens/Dashboard/Dashboard.dart';
import 'package:my_family/Screens/FirstLogin/Login.dart';
import 'package:splashscreen/splashscreen.dart';

import 'commons/theme.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColorBrightness: Brightness.light,
        primarySwatch: Colors.brown,
        primaryColor: Color(0xffFE6A16),
        accentColor: primaryColor,
        textTheme: TextTheme(
          headline1: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.bold,
            color: Color(0xff231C2D),
          ),
          headline2: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w700,
            color: Color(0xff231C2D),
          ),
          headline3: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w700,
            color: Color(0xff231C2D),
          ),
          bodyText2: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.normal,
            color: Color(0xff656565),
          ),
        ),
        textButtonTheme: TextButtonThemeData(
          style: ButtonStyle(
            overlayColor: MaterialStateProperty.all(
              primaryColor.withOpacity(0.3),
            ),
          ),
        ),
        textSelectionTheme: TextSelectionThemeData(
          cursorColor: primaryColor,
        ),
        inputDecorationTheme: InputDecorationTheme(
          labelStyle: TextStyle(
            color: Color(0xff656565),
            fontSize: 14,
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Color(0xffFE6A16)),
          ),
          contentPadding: EdgeInsets.all(4),
          focusColor: Color(0xff656565),
          isDense: true,
        ),
      ),
      home: Splash(),
    );
  }
}

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 3,
      navigateAfterSeconds: true ? Login() : DashBoard(),
      backgroundColor: Colors.white,
      styleTextUnderTheLoader: new TextStyle(),
      photoSize: 100,
      image: Image.asset("assets/splash.png"),
      imageBackground: AssetImage("assets/background.png"),
      useLoader: false,
    );
  }
}
