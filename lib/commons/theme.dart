import 'package:flutter/material.dart';

const primaryColor = Color(0xffFE6A16);

const contentPadding = EdgeInsets.symmetric(horizontal: 20);

InputDecoration inputDecoration({String labelText, String hintText}) {
  return InputDecoration(
    labelText: labelText,
    hintText: hintText,
  );
}

InputDecoration inputNoOutlineDecoration({String hintText}) {
  return InputDecoration(
    border: OutlineInputBorder(
      borderSide: BorderSide.none,
    ),
    contentPadding: contentPadding,
    hintText: hintText,
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide.none,
    ),
  );
}
