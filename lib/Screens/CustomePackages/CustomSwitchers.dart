import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ButtonSwitch extends StatelessWidget {
  String title;
  bool selected;
  Function onPress;
  ButtonSwitch({this.title, this.selected, this.onPress});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 7.6),
      child: TextButton(
        onPressed: onPress,
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(this.selected ? Theme.of(context).primaryColor : Color(0xffE1E1E1)),
        ),
        child: Text(
          this.title,
          style: TextStyle(color: this.selected ? Colors.white : Color(0xff656565)),
        ),
      ),
    );
  }
}
