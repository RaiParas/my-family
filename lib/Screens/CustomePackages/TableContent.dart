import 'package:flutter/material.dart';

class TabulatedInfo extends StatelessWidget {
  IconData icon;
  String title, info;
  Function onPress;
  TabulatedInfo({this.icon, this.title, this.info, this.onPress});
  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: this.onPress,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            children: [
              Icon(
                this.icon,
                color: Color(0xFF1A1919),
              ),
              SizedBox(width: 18),
              Expanded(
                  child: Text(
                this.title,
                style: TextStyle(color: Color(0xff656565), fontWeight: FontWeight.w600, fontSize: 14),
              )),
              Text(
                this.info,
                style: TextStyle(color: Color(0xff656565), fontWeight: FontWeight.w600, fontSize: 12),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
