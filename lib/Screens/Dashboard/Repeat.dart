import 'package:flutter/material.dart';
import 'package:my_family/Screens/CustomePackages/CustomSwitchers.dart';
import 'package:my_family/commons/theme.dart';

class Repeater extends StatefulWidget {
  @override
  _RepeaterState createState() => _RepeaterState();
}

class _RepeaterState extends State<Repeater> {
  String repeat = "";
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: Container(
          color: Colors.white,
          padding: contentPadding,
          margin: contentPadding,
          child: Wrap(
            children: [
              Text("Set as Repeat Task"),
              Wrap(
                direction: Axis.horizontal,
                children: [
                  ButtonSwitch(
                    selected: repeat == "Daily",
                    onPress: () {
                      setState(() {
                        repeat = "Daily";
                      });
                    },
                    title: "Daily",
                  ),
                  ButtonSwitch(
                    selected: repeat == "Weekly",
                    onPress: () {
                      setState(() {
                        repeat = "Weekly";
                      });
                    },
                    title: "Weekly",
                  ),
                  ButtonSwitch(
                    selected: repeat == "Monthly",
                    onPress: () {
                      setState(() {
                        repeat = "Monthly";
                      });
                    },
                    title: "Monthly",
                  ),
                  ButtonSwitch(
                    selected: repeat == "Yearly",
                    onPress: () {
                      setState(() {
                        repeat = "Yearly";
                      });
                    },
                    title: "Yearly",
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      "Cancel",
                      style: TextStyle(
                        fontWeight: FontWeight.w300,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      "Done",
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
