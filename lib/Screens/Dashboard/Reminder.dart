import 'package:flutter/material.dart';
import 'package:my_family/commons/theme.dart';

class Reminder extends StatefulWidget {
  @override
  _RemanderState createState() => _RemanderState();
}

class _RemanderState extends State<Reminder> {
  String reminder = "";
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: Container(
          margin: EdgeInsets.all(10),
          padding: contentPadding,
          color: Colors.white,
          child: Wrap(
            children: [
              Text("Reminder"),
              Row(
                children: [
                  Checkbox(
                      value: reminder == "No reminder",
                      onChanged: (changed) {
                        setState(() {
                          reminder = "No reminder";
                        });
                      }),
                  Text("No reminder"),
                ],
              ),
              Row(
                children: [
                  Checkbox(
                      value: reminder == "Same with due date",
                      onChanged: (changed) {
                        setState(() {
                          reminder = "Same with due date";
                        });
                      }),
                  Text("Same with due date"),
                ],
              ),
              Row(
                children: [
                  Checkbox(
                      value: reminder == "5 minutes before",
                      onChanged: (changed) {
                        setState(() {
                          reminder = "5 minutes before";
                        });
                      }),
                  Text("5 minutes before"),
                ],
              ),
              Row(
                children: [
                  Checkbox(
                      value: reminder == "10 minutes before",
                      onChanged: (changed) {
                        setState(() {
                          reminder = "10 minutes before";
                        });
                      }),
                  Text("10 minutes before"),
                ],
              ),
              Row(
                children: [
                  Checkbox(
                      value: reminder == "15 minutes before",
                      onChanged: (changed) {
                        setState(() {
                          reminder = "15 minutes before";
                        });
                      }),
                  Text("15 minutes before"),
                ],
              ),
              Row(
                children: [
                  Checkbox(
                      value: reminder == "30 minutes before",
                      onChanged: (changed) {
                        setState(() {
                          reminder = "30 minutes before";
                        });
                      }),
                  Text("30 minutes before"),
                ],
              ),
              Row(
                children: [
                  Checkbox(
                      value: reminder == "1 day before",
                      onChanged: (changed) {
                        setState(() {
                          reminder = "1 day before";
                        });
                      }),
                  Text("1 day before"),
                ],
              ),
              Row(
                children: [
                  Checkbox(
                      value: reminder == "2 days before",
                      onChanged: (changed) {
                        setState(() {
                          reminder = "2 days before";
                        });
                      }),
                  Text("2 days before"),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      "Cancel",
                      style: TextStyle(
                        fontWeight: FontWeight.w300,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      "Done",
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
