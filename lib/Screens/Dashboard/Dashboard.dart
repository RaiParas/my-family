import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_rounded_date_picker/flutter_rounded_date_picker.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:my_family/Screens/CustomePackages/CustomSwitchers.dart';
import 'package:my_family/Screens/CustomePackages/TableContent.dart';
import 'package:my_family/Screens/Dashboard/DueDate.dart';
import 'package:my_family/commons/theme.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:table_calendar/table_calendar.dart';

class ListItem {
  int value;
  String name;

  ListItem(this.value, this.name);
}

class DashBoard extends StatelessWidget {
  List<ListItem> _dropdownItems = [ListItem(1, "First Value"), ListItem(2, "Second Item"), ListItem(3, "Third Item"), ListItem(4, "Fourth Item")];

  List<DropdownMenuItem<ListItem>> buildDropDownMenuItems(List listItems) {
    List<DropdownMenuItem<ListItem>> items = List();
    for (ListItem listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Text(listItem.name),
          value: listItem,
        ),
      );
    }
    return items;
  }

  List<DropdownMenuItem<ListItem>> _dropdownMenuItems;
  ListItem _selectedItem;

  DashBoard() {
    _dropdownMenuItems = buildDropDownMenuItems(_dropdownItems);
    _selectedItem = _dropdownMenuItems[0].value;
  }

  List<String> filters = [
    "All",
    "Work",
    "Personal",
    "Wishlist",
    "Family",
    "None",
  ];

  List _elements = [
    {'name': 'Virat Kohli', 'group': 'Today', 'time': "6:00AM", 'asigns': [], "isFav": false},
    {'name': 'Rohit Sharma', 'group': 'Today', 'time': "6:00AM", 'asigns': [], "isFav": false},
    {
      'name': 'AB de Villiers',
      'group': 'Tomorrow',
      'time': "6:00AM",
      'asigns': [],
      "isFav": false,
    },
    {
      'name': 'Jasprit Bumrah',
      'group': 'Others',
      'time': "6:00AM",
      'asigns': [],
      "isFav": false,
    },
    {
      'name': 'AB de Villiers',
      'group': 'Tomorrow',
      'time': "6:00AM",
      'asigns': [],
      "isFav": false,
    },
    {
      'name': 'Jasprit Bumrah',
      'group': 'Others',
      'time': "6:00AM",
      'asigns': [],
      "isFav": false,
    },
    {
      'name': 'AB de Villiers',
      'group': 'Tomorrow',
      'time': "6:00AM",
      'asigns': [],
      "isFav": false,
    },
    {
      'name': 'Jasprit Bumrah',
      'group': 'Others',
      'time': "6:00AM",
      'asigns': [],
      "isFav": false,
    },
  ];

  List<Map<String, dynamic>> circles = [
    {"name": "Family1", "image": "", "members": "3"},
    {"name": "Family2", "image": "", "members": "2"},
    {"name": "Family3", "image": "", "members": "4"},
  ];

  PageController _controller = PageController(
    initialPage: 0,
  );

  CalendarController _calendarController = CalendarController();

  GlobalKey<ScaffoldState> _dashboard = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _dashboard,
      drawerEnableOpenDragGesture: true,
      body: Container(
        width: double.infinity,
        padding: contentPadding,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/background.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: SafeArea(
          child: Column(
            children: [
              Row(
                children: [
                  Image(
                    width: 18,
                    image: AssetImage("assets/avatar17.png"),
                  ),
                  Expanded(
                    child: Wrap(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: TextButton(
                            child: SizedBox(
                              width: 90,
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Family",
                                    style: TextStyle(
                                      color: Color(0xff231C2D),
                                    ),
                                  ),
                                  Icon(
                                    Icons.arrow_drop_down,
                                    color: Color(0xff231C2D),
                                  ),
                                ],
                              ),
                            ),
                            onPressed: () {
                              showCupertinoModalBottomSheet(
                                context: context,
                                elevation: 4,
                                barrierColor: Colors.black.withOpacity(0.5),
                                builder: (context) => Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(16),
                                      topRight: Radius.circular(16),
                                    ),
                                  ),
                                  child: ConstrainedBox(
                                    constraints: new BoxConstraints(
                                      minHeight: 100.0,
                                      maxHeight: MediaQuery.of(context).size.height * 0.76,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                        vertical: 20.0,
                                      ),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Center(
                                            child: Image(
                                              width: 54,
                                              image: AssetImage("assets/line.png"),
                                            ),
                                          ),
                                          Center(
                                            child: Padding(
                                              padding: const EdgeInsets.only(top: 16.0),
                                              child: Text(
                                                "Switch your circle",
                                                style: Theme.of(context).textTheme.headline2,
                                              ),
                                            ),
                                          ),
                                          Material(
                                            child: ListView.builder(
                                              padding: EdgeInsets.zero,
                                              shrinkWrap: true,
                                              itemCount: circles.length,
                                              itemBuilder: (BuildContext context, int index) {
                                                return ListTile(
                                                  leading: Icon(
                                                    Icons.add_circle_outline,
                                                    size: 50,
                                                  ),
                                                  title: Text(
                                                    circles[index]['name'],
                                                    style: Theme.of(context).textTheme.headline3,
                                                  ),
                                                  subtitle: Text(
                                                    "${circles[index]['members']} Members",
                                                  ),
                                                  onTap: () {},
                                                );
                                              },
                                            ),
                                          ),
                                          Material(
                                            child: ListTile(
                                              leading: Icon(
                                                Icons.add_circle_outline,
                                                size: 50,
                                              ),
                                              title: Text(
                                                "Create a New Circle",
                                                style: Theme.of(context).textTheme.headline3,
                                              ),
                                              subtitle: Text(
                                                "For family, friends, caregivers and more ",
                                              ),
                                              onTap: () {},
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  IconButton(
                    icon: Icon(CupertinoIcons.gear_alt_fill),
                    onPressed: () {},
                  )
                ],
              ),
              Container(
                padding: EdgeInsets.only(bottom: 10),
                height: 40,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: ListView.builder(
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 7.0),
                            child: TextButton(
                              onPressed: () {},
                              child: Text(
                                this.filters[index],
                                style: TextStyle(color: Color(0xff656565)),
                              ),
                              style: TextButton.styleFrom(
                                alignment: Alignment.center,
                                backgroundColor: Theme.of(context).accentColor.withOpacity(0.4),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(38.0),
                                ),
                                padding: EdgeInsets.symmetric(horizontal: 12, vertical: 2),
                              ),
                            ),
                          );
                        },
                        itemCount: this.filters.length,
                        scrollDirection: Axis.horizontal,
                      ),
                    ),
                    IconButton(
                        icon: Image(
                          image: AssetImage("assets/filter.png"),
                        ),
                        onPressed: () {})
                  ],
                ),
              ),
              Expanded(
                child: false
                    ? Center(
                        child: Image(
                          width: 135,
                          height: 170.5,
                          image: AssetImage("assets/noItems.png"),
                        ),
                      )
                    : GroupedListView<dynamic, String>(
                        elements: _elements,
                        groupBy: (element) => element['group'],
                        groupComparator: (value1, value2) => value2.compareTo(value1),
                        itemComparator: (item1, item2) => item1['group'].compareTo(item2['group']),
                        order: GroupedListOrder.ASC,
                        useStickyGroupSeparators: false,
                        groupSeparatorBuilder: (String value) => Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: Text(
                            value,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 20,
                              color: Color(0xff656565),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        itemBuilder: (c, element) {
                          return Container(
                            margin: new EdgeInsets.symmetric(
                              vertical: 6.0,
                            ),
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Color(0xffE9E9E9),
                                width: 1,
                              ),
                              color: Color(0xffF5F9FC),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: ListTile(
                              contentPadding: EdgeInsets.symmetric(
                                horizontal: 0,
                                vertical: 2,
                              ),
                              leading: CircularCheckBox(
                                activeColor: true ? Theme.of(context).accentColor : Color(0xffA5A5A5),
                                value: true,
                                materialTapTargetSize: MaterialTapTargetSize.padded,
                                onChanged: (bool x) {},
                              ),
                              title: Text(element['name']),
                              subtitle: Row(
                                children: [
                                  Text("Onw"),
                                  Material(
                                    borderRadius: BorderRadius.circular(24),
                                    child: IconButton(
                                      splashRadius: 24,
                                      icon: Icon(
                                        CupertinoIcons.list_dash,
                                        size: 15,
                                      ),
                                      onPressed: () {},
                                    ),
                                  ),
                                  Material(
                                    borderRadius: BorderRadius.circular(24),
                                    child: IconButton(
                                      splashRadius: 24,
                                      icon: Icon(
                                        CupertinoIcons.bell_fill,
                                        size: 15,
                                      ),
                                      onPressed: () {},
                                    ),
                                  ),
                                ],
                              ),
                              trailing: Material(
                                borderRadius: BorderRadius.circular(24),
                                child: IconButton(
                                  splashRadius: 24,
                                  icon: Icon(CupertinoIcons.star),
                                  onPressed: () {},
                                ),
                              ),
                            ),
                          );
                        },
                      ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(bottom: 0.0),
        child: FloatingActionButton(
          child: Icon(
            Icons.add,
          ),
          onPressed: () {
            showCupertinoModalBottomSheet(
              context: context,
              elevation: 4,
              barrierColor: Colors.black.withOpacity(0.5),
              builder: (context) => Container(
                height: MediaQuery.of(context).size.height * 0.76,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(16),
                    topRight: Radius.circular(16),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 20.0,
                  ),
                  child: Material(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Center(
                          child: Image(
                            width: 54,
                            image: AssetImage("assets/line.png"),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 16.0),
                          child: TextField(
                            decoration: inputNoOutlineDecoration(
                              hintText: "What would you like to do?",
                            ),
                          ),
                        ),
                        Padding(
                          padding: contentPadding,
                          child: Row(
                            children: [
                              Expanded(
                                child: TextButton.icon(
                                  onPressed: () {
                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return Center(
                                          child: Container(
                                            width: double.infinity,
                                            margin: contentPadding,
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.circular(10),
                                            ),
                                            child: Material(
                                              color: Colors.transparent,
                                              child: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  Container(
                                                    alignment: Alignment.topLeft,
                                                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                                                    child: Text(
                                                      "Assign to",
                                                      style: TextStyle(
                                                        color: Color(0xff231C2D),
                                                        fontSize: 18,
                                                        fontWeight: FontWeight.w700,
                                                      ),
                                                    ),
                                                  ),
                                                  Container(
                                                    padding: contentPadding,
                                                    margin: EdgeInsets.only(bottom: 20, left: 20, right: 20),
                                                    decoration: BoxDecoration(
                                                      color: Color(0xffDFDFDF),
                                                      borderRadius: BorderRadius.circular(21),
                                                    ),
                                                    child: TextField(
                                                      decoration: InputDecoration(
                                                        hintText: "Search",
                                                        contentPadding: EdgeInsets.symmetric(vertical: 11),
                                                        border: InputBorder.none,
                                                        focusedBorder: InputBorder.none,
                                                      ),
                                                    ),
                                                  ),
                                                  UserSelectionBox(
                                                    image: "assets/avatar1.png",
                                                    name: "Suzam  Shrestha",
                                                  ),
                                                  UserSelectionBox(
                                                    image: "assets/avatar2.png",
                                                    name: "Roshan Pant",
                                                  ),
                                                  UserSelectionBox(
                                                    image: "assets/avatar3.png",
                                                    name: "Paras Rai",
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                    );
                                  },
                                  label: Text(
                                    "Unassigned",
                                    style: Theme.of(context).textTheme.bodyText2,
                                  ),
                                  icon: Icon(
                                    CupertinoIcons.person_alt_circle_fill,
                                    color: Color(0xff656565),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: TextButton.icon(
                                  onPressed: () {
                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return DueDate();
                                      },
                                    );
                                  },
                                  label: Text(
                                    "Due Date",
                                    style: Theme.of(context).textTheme.bodyText2,
                                  ),
                                  icon: Icon(
                                    Icons.calendar_today,
                                    color: Color(0xff656565),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Container(
                            padding: contentPadding,
                            alignment: Alignment.topLeft,
                            child: ListView(
                              children: [
                                TextField(
                                  decoration: inputNoOutlineDecoration(
                                    hintText: "Description",
                                  ),
                                ),
                                TextButton.icon(
                                  onPressed: () {},
                                  style: ButtonStyle(alignment: Alignment.centerLeft),
                                  label: Text(
                                    "Add Subtask",
                                    style: TextStyle(
                                      color: Color(0xffACACAC),
                                    ),
                                  ),
                                  icon: Icon(
                                    Icons.add,
                                    color: Color(0xffACACAC),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Material(
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              IconButton(
                                icon: Icon(Icons.add_box),
                                onPressed: () {
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return Center(
                                        child: Container(
                                          width: double.infinity,
                                          margin: contentPadding,
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(10),
                                          ),
                                          child: Material(
                                            color: Colors.transparent,
                                            child: Column(
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                PriorityBox(color: Color(0xffF63B3B), borderColor: Color(0xffF63B3B), txt: "High priority"),
                                                PriorityBox(color: Color(0xff4B63FB), borderColor: Color(0xff283DBF), txt: "Low priority"),
                                                PriorityBox(color: Color(0xffF69F3B), borderColor: Color(0xffDB8119), txt: "Medium priority"),
                                                PriorityBox(color: Color(0xffB2B2B2), borderColor: Color(0xff949494), txt: "No priority"),
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  );
                                },
                              ),
                              IconButton(
                                icon: Icon(Icons.attachment),
                                onPressed: () {},
                              ),
                              IconButton(
                                icon: Icon(CupertinoIcons.photo),
                                onPressed: () {},
                              ),
                              Expanded(child: SizedBox()),
                              TextButton(
                                child: Text(
                                  "Create",
                                  style: TextStyle(color: Theme.of(context).accentColor),
                                ),
                                onPressed: () {},
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
      drawer: Drawer(
        child: DrawerHeader(
          child: Container(),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          padding: contentPadding,
          height: 40,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                icon: Icon(CupertinoIcons.text_alignleft),
                onPressed: () {
                  _dashboard.currentState.openDrawer();
                },
              ),
              IconButton(
                icon: Icon(
                  Icons.check_box,
                  color: Theme.of(context).accentColor,
                ),
                onPressed: () {},
              ),
              IconButton(
                icon: Icon(Icons.calendar_today_outlined),
                onPressed: () {},
              ),
              IconButton(
                icon: Icon(CupertinoIcons.person_2_fill),
                onPressed: () {},
              ),
              IconButton(
                icon: Icon(CupertinoIcons.calendar_badge_plus),
                onPressed: () {},
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class PriorityBox extends StatelessWidget {
  Color color, borderColor;
  String txt;
  PriorityBox({this.color, this.borderColor, this.txt = "High priority"});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 12,
          horizontal: 20,
        ),
        child: Row(
          children: [
            Container(
              width: 14,
              height: 14,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                color: this.color,
                border: Border.all(
                  color: borderColor,
                  width: 1,
                ),
              ),
            ),
            SizedBox(
              width: 12,
            ),
            Text(
              txt,
              style: TextStyle(
                fontSize: 14,
                color: Color(0xff656565),
              ),
            ),
          ],
        ),
      ),
      onTap: () {},
    );
  }
}

class UserSelectionBox extends StatelessWidget {
  String image, name;
  UserSelectionBox({this.image, this.name = "High priority"});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 12,
          horizontal: 20,
        ),
        child: Row(
          children: [
            Container(
              width: 36,
              height: 36,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(18),
                border: Border.all(
                  color: Color(0xffC5C5C5),
                  width: 1,
                ),
              ),
              child: ClipRRect(
                child: Image.asset(image),
                borderRadius: BorderRadius.circular(18),
              ),
            ),
            SizedBox(
              width: 12,
            ),
            Text(
              name,
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w700,
                color: Color(0xff231C2D),
              ),
            ),
          ],
        ),
      ),
      onTap: () {},
    );
  }
}
