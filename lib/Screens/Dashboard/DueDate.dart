import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_rounded_date_picker/flutter_rounded_date_picker.dart';
import 'package:my_family/Screens/CustomePackages/CustomSwitchers.dart';
import 'package:my_family/Screens/CustomePackages/TableContent.dart';
import 'package:my_family/Screens/Dashboard/Reminder.dart';
import 'package:my_family/Screens/Dashboard/Repeat.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class DueDate extends StatefulWidget {
  @override
  _DueDateState createState() => _DueDateState();
}

class _DueDateState extends State<DueDate> {
  String day = "";

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: new BoxConstraints(
          minWidth: double.infinity,
          maxHeight: MediaQuery.of(context).size.height * 0.8,
        ),
        child: Padding(
          padding: EdgeInsets.all(20),
          child: DecoratedBox(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Material(
              color: Colors.transparent,
              child: DefaultTabController(
                length: 2,
                child: Column(
                  children: [
                    TabBar(
                      tabs: [
                        Tab(child: Text("Date")),
                        Tab(child: Text("Duration")),
                      ],
                    ),
                    Expanded(
                      child: Material(
                        child: TabBarView(
                          children: [
                            Container(
                              child: ListView(
                                children: [
                                  AspectRatio(
                                    aspectRatio: 1,
                                    child: SfDateRangePicker(
                                      selectionMode: DateRangePickerSelectionMode.single,
                                      enablePastDates: false,
                                      showNavigationArrow: true,
                                      view: DateRangePickerView.month,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              child: ListView(
                                children: [
                                  AspectRatio(
                                    aspectRatio: 1,
                                    child: SfDateRangePicker(
                                      enablePastDates: false,
                                      selectionMode: DateRangePickerSelectionMode.range,
                                      view: DateRangePickerView.month,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        ButtonSwitch(
                          title: "Today",
                          selected: day == "Today",
                          onPress: () {
                            setState(() {
                              day = "Today";
                            });
                          },
                        ),
                        ButtonSwitch(
                          title: "Tomorrow",
                          selected: day == "Tomorrow",
                          onPress: () {
                            setState(() {
                              day = "Tomorrow";
                            });
                          },
                        ),
                        ButtonSwitch(
                          title: "2 Days later",
                          selected: day == "2 Days later",
                          onPress: () {
                            setState(() {
                              day = "2 Days later";
                            });
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    TabulatedInfo(
                      icon: CupertinoIcons.clock_fill,
                      title: "Time",
                      info: "No",
                      onPress: () async {
                        final timePicked = await showRoundedTimePicker(
                          context: context,
                          theme: Theme.of(context).copyWith(
                            primaryColor: Colors.white,
                            textButtonTheme: TextButtonThemeData(
                                style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(Colors.black),
                                    foregroundColor: MaterialStateProperty.all(Theme.of(context).primaryColor))),
                          ),
                          initialTime: TimeOfDay.now(),
                        );
                      },
                    ),
                    TabulatedInfo(
                      icon: CupertinoIcons.bell_fill,
                      title: "Remainer",
                      info: "No",
                      onPress: () async {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return Reminder();
                          },
                        );
                      },
                    ),
                    TabulatedInfo(
                      icon: Icons.repeat,
                      title: "Repeat",
                      info: "No",
                      onPress: () async {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return Repeater();
                          },
                        );
                      },
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Cancel",
                            style: TextStyle(
                              fontWeight: FontWeight.w300,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        ),
                        TextButton(
                          onPressed: () {},
                          child: Text(
                            "Done",
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
