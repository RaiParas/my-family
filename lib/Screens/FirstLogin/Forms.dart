import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_family/commons/theme.dart';

var avatars = [];

class Forms extends StatefulWidget {
  @override
  _FormsState createState() => _FormsState();
}

class _FormsState extends State<Forms> {
  Map<String, dynamic> selectedAvater = {'path': "", 'image': ""};
  @override
  void initState() {
    super.initState();
    for (int i = 1; i < 26; i++) {
      avatars.add("assets/avatar${i.toString()}.png");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/background.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            children: [
              Expanded(
                child: PageView(
                  children: [
                    NameInput(
                      selectedAvatar: this.selectedAvater['path'],
                      onTap: (String imageSrc) {
                        var image = AssetImage(imageSrc);
                        var data = {'path': imageSrc, 'image': image};
                        this.setState(() {
                          selectedAvater = data;
                        });
                      },
                    ),
                    CircleInput(
                      selectedAvatar: this.selectedAvater['path'],
                      onTap: (String imageSrc) {
                        var image = AssetImage(imageSrc);
                        var data = {'path': imageSrc, 'image': image};
                        this.setState(() {
                          selectedAvater = data;
                        });
                      },
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                      onPressed: () {},
                      child: Row(
                        children: [
                          Text(
                            "Next",
                            style: TextStyle(color: Colors.white),
                          ),
                          Icon(
                            CupertinoIcons.forward,
                            color: Colors.white,
                          ),
                        ],
                      ),
                      style: TextButton.styleFrom(
                        alignment: Alignment.center,
                        backgroundColor: Theme.of(context).accentColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(38.0),
                        ),
                        padding:
                            EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}

class NameInput extends StatelessWidget {
  var selectedAvatar = "";
  Function onTap = () {};
  NameInput({this.selectedAvatar, this.onTap});
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: contentPadding,
        child: Form(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 40.0, bottom: 45),
                child: Text(
                  "Hello!",
                  style: Theme.of(context).textTheme.headline1,
                ),
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    child: TextField(
                      decoration: inputDecoration(
                        labelText: "First Name",
                        hintText: "Enter First Name",
                      ),
                    ),
                  ),
                  SizedBox(width: 25),
                  Expanded(
                    child: TextField(
                      decoration: inputDecoration(
                        labelText: "First Name",
                        hintText: "Enter First Name",
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                child: TextField(
                  decoration: inputDecoration(
                    labelText: "Nick Name",
                    hintText: "Enter Nick Name",
                  ),
                ),
              ),
              Text(
                "Choose your avatar",
                style: Theme.of(context).textTheme.headline2,
              ),
              Padding(
                padding: EdgeInsets.only(top: 25, bottom: 3),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Ink(
                      decoration: BoxDecoration(
                        color: Color(0xff656565),
                        borderRadius: BorderRadius.circular(33),
                      ),
                      child: InkWell(
                        onTap: () {},
                        borderRadius: BorderRadius.circular(33),
                        child: Container(
                          width: 66,
                          height: 66,
                          child: Center(
                            child: Icon(
                              CupertinoIcons.photo,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 11,
                    ),
                    Text(
                      "Upload from gallery",
                      style: Theme.of(context).textTheme.headline2,
                    )
                  ],
                ),
              ),
              Expanded(
                child: GridView.builder(
                    padding: EdgeInsets.symmetric(vertical: 20),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      childAspectRatio: 1,
                      crossAxisSpacing: 15,
                      crossAxisCount: 5,
                    ),
                    itemCount: avatars.length,
                    itemBuilder: (BuildContext ctx, index) {
                      return Container(
                        width: 49,
                        height: 49,
                        child: Stack(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: this.selectedAvatar == avatars[index]
                                        ? Colors.orange
                                        : Colors.transparent,
                                    width: 2,
                                  ),
                                  borderRadius: BorderRadius.circular(100),
                                ),
                                child: Ink.image(
                                  image: AssetImage(avatars[index]),
                                  width: 49,
                                  height: 49,
                                  fit: BoxFit.cover,
                                  child: InkWell(
                                    borderRadius: BorderRadius.circular(100),
                                    onTap: () {
                                      this.onTap(avatars[index]);
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              top: 0,
                              right: 0,
                              child: Container(
                                width: 24,
                                height: 24,
                                child: this.selectedAvatar == avatars[index]
                                    ? Image(
                                        width: 24,
                                        height: 24,
                                        image: AssetImage(
                                            "assets/selectedIcon.png"),
                                      )
                                    : null,
                              ),
                            )
                          ],
                        ),
                      );
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CircleInput extends StatelessWidget {
  var selectedAvatar = "";
  Function onTap = () {};
  CircleInput({this.selectedAvatar, this.onTap});
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: contentPadding,
        child: Form(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 40.0, bottom: 15),
                child: Text(
                  "Create your \nCircle!",
                  style: Theme.of(context).textTheme.headline1,
                ),
              ),
              TextField(
                decoration: inputDecoration(
                  labelText: "Create your group",
                  hintText: "Enter group name",
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 40.0),
                child: Text(
                  "Choose your avatar",
                  style: Theme.of(context).textTheme.headline2,
                ),
              ),
              Expanded(
                child: GridView.builder(
                    padding: EdgeInsets.symmetric(vertical: 20),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      childAspectRatio: 1,
                      crossAxisSpacing: 15,
                      crossAxisCount: 5,
                    ),
                    itemCount: avatars.length,
                    itemBuilder: (BuildContext ctx, index) {
                      return Container(
                        width: 49,
                        height: 49,
                        child: Stack(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: this.selectedAvatar == avatars[index]
                                        ? Colors.orange
                                        : Colors.transparent,
                                    width: 2,
                                  ),
                                  borderRadius: BorderRadius.circular(100),
                                ),
                                child: Ink.image(
                                  image: AssetImage(avatars[index]),
                                  width: 49,
                                  height: 49,
                                  fit: BoxFit.cover,
                                  child: InkWell(
                                    borderRadius: BorderRadius.circular(100),
                                    onTap: () {
                                      this.onTap(avatars[index]);
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              top: 0,
                              right: 0,
                              child: Container(
                                width: 24,
                                height: 24,
                                child: this.selectedAvatar == avatars[index]
                                    ? Image(
                                        width: 24,
                                        height: 24,
                                        image: AssetImage(
                                            "assets/selectedIcon.png"),
                                      )
                                    : null,
                              ),
                            )
                          ],
                        ),
                      );
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
