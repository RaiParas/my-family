import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:my_family/Screens/FirstLogin/Forms.dart';
import 'package:my_family/commons/theme.dart';

var avatars = [];

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  Map<String, dynamic> selectedAvater = {'path': "", 'image': ""};
  @override
  void initState() {
    super.initState();
    for (int i = 1; i < 26; i++) {
      avatars.add("assets/avatar${i.toString()}.png");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/background.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: GoogleSigin(),
      ),
    );
  }
}

class GoogleSigin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset("assets/loginCalender.png", width: 282, height: 256),
        Padding(
          padding: const EdgeInsets.only(top: 42.0, bottom: 18),
          child: Text(
            "Organize your family life",
            style: Theme.of(context).textTheme.headline1,
          ),
        ),
        Text(
          "TaskWall lets you organize the whole family's schedule, activities and to-do lists in one simple space.",
          textAlign: TextAlign.center,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 100.0),
          child: Image.asset("assets/dots.png", width: 80, height: 13),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 20.0),
          child: TextButton(
              onPressed: () async {
                GoogleSignIn _googleSignIn = GoogleSignIn(
                  scopes: ['email'],
                );

                var data = await _googleSignIn.signIn();
                print(data);
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) => Forms()),
                // );
              },
              child: Text("Continue with google")),
        )
      ],
    );
  }
}
